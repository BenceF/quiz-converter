;;;; Eval this in the README.org buffer

(lexical-let ((ghci-buffer (get-buffer "*quiz*")))
  (unless ghci-buffer
    (setq ghci-buffer
          (make-comint "quiz" "ghciWith" (not 'startfile)
                       ;; arguments
                       "xml-conduit" "aeson-pretty" "hlint")))
  (setq-local org-babel-pre-tangle-hook nil)
  (add-hook
   'write-file-functions
   #'(lambda ()
       (org-babel-tangle)
       (comint-send-string
        ghci-buffer
        (concat ":l Quiz.hs\n"
                ":! hlint Quiz.hs\n"))
       nil)
   'append
   'local))
