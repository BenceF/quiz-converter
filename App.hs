{-# Language OverloadedStrings #-}
{-# Language ScopedTypeVariables #-}

module Main where

import qualified Data.Map as M
import           Data.Text hiding (head)
import           Prelude hiding (readFile)
import           Text.XML
import           Text.XML.Cursor

rootCursorFrom :: FilePath -> IO Cursor
rootCursorFrom path = do
  doc <- readFile def path
  return $ fromDocument doc

inNs :: Text -> Name
inNs name = Name name (Just "http://www.imsglobal.org/xsd/ims_qtiasiv1p2") Nothing

quiz :: Axis
quiz = element $ inNs "assessment"

question :: Axis
question = element (inNs "item") >=> attributeIs "title" "Question"

data Quiz = Quiz { quizTitle :: Text
                 , quizQuestions :: [Question]
                 } deriving Show

data Question = Question { questionText :: Text
                         , possibleAnswers :: M.Map Char Text
                         , correctAnswers :: [Char]
                         } deriving Show

parseQuiz :: Cursor -> Quiz
parseQuiz quiz = let title = (head $ attribute "title" quiz)
                     questions = fmap parseQuetion (quiz $// question)
                 in Quiz title questions

parseQuetion :: Cursor -> Question
parseQuetion = undefined

main = do
  cursor <- rootCursorFrom "assessment_qti.xml"
  let quizes = (cursor $// quiz)
  return $ fmap parseQuiz quizes
