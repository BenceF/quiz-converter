{-# Language OverloadedStrings #-}

module Main where

import Prelude hiding (putStrLn)
import Data.Text
import Data.Text.IO (putStrLn)
import Text.XML
import Text.XML.Cursor

import Quiz

getAllQuestionText :: Cursor -> [Text]
getAllQuestionText root = let firstQuiz = case root $/ quiz of
                                [] -> error "No quiz found"
                                q:_ -> q
                              allQuestions = case firstQuiz $// selectQuestion of
                                [] -> error "No questions found"
                                q -> q
                            in fmap questionText allQuestions

firstAnswerDict :: Cursor -> AnswerDict
firstAnswerDict root = let firstQuiz = case root $/ quiz of
                             [] -> error "No quiz found"
                             q:_ -> q
                           firstQuestion = case firstQuiz $// selectQuestion of
                             [] -> error "No questions found"
                             q:_ -> q
                       in readAnswers firstQuestion

firstCorrectAnswer :: Cursor -> [Int]
firstCorrectAnswer root = let firstQuiz = case root $/ quiz of
                                [] -> error "No quiz found"
                                q:_ -> q
                              firstQuestion = case firstQuiz $// selectQuestion of
                                [] -> error "No questions found"
                                q:_ -> q
                              cardinality = readCardinality firstQuestion
                          in getRightAnswers firstQuestion cardinality

thirdCorrectAnswers :: Cursor -> [Int]
thirdCorrectAnswers root =
  let firstQuiz = case root $/ quiz of
        [] -> error "No quiz found"
        q:_ -> q
      thirdQuestion = case firstQuiz $// selectQuestion of
        q1:q2:q3:_ -> q3
        _ -> error "No third question found"
      cardinality = readCardinality thirdQuestion
  in getRightAnswers thirdQuestion cardinality

main :: IO ()
main = do
  root <- rootCursorFrom "assessment_qti.xml"
  mapM_ putStrLn $ getAllQuestionText root
  -- print $ dict (firstAnswerDict root)
  -- print $ firstCorrectAnswer root
  -- print $ thirdCorrectAnswers root
  print $ fmap extractQuiz (root $/ quiz)
